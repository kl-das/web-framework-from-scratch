# frozen_string_literal: true

require_relative './framework'

APP = App.new do
  get '/' do             # :route_spec
    'This is the root!'  # :block
  end

  get '/users/:username' do
    'This is a user!'
  end
end
