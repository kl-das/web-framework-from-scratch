# frozen_string_literal: true

# Framework code which app users won't have to see.
class App
  def initialize(&block)
    @routes = RouteTable.new(block)
  end

  def call(env)
    request = Rack::Request.new(env)

    @routes.each do |route|
      content = route.match(request)
      return [200, {}, [content]] if content
    end

    # Catch all if no route_spec in @route matches
    [404, {}, ['Route not found!']]
  end

  # rubocop:disable Style/Documentation
  class RouteTable
    def initialize(block)
      @routes = []
      instance_eval(&block)
    end

    def get(route_spec, &block)
      @routes << Route.new(route_spec, block)
      puts @routes
    end

    def each(&block)
      @routes.each(&block)
    end
  end

  # rubocop:disable Style/StructInheritance
  class Route < Struct.new(:route_spec, :block)
    def match(request)
      return block.call if request.path.eql? route_spec

      nil
    end
  end

  # rubocop:enable Style/Documentation, Style/StructInheritance
end
